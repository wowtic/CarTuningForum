﻿using System;
using System.Collections.Generic;

namespace CarTuningForum.Models
{
    public class Post
    { 
        public int Id { get; set; }
        public string AuthorId { get; set; }
        public User Author { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public int Likes { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedOn { get; set; }
        public IEnumerable<Comment> Comments { get; set; } = new List<Comment>();
        public IEnumerable<Reply> Replies { get; set; } = new List<Reply>();
    }
}
