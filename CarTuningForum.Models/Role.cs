﻿namespace CarTuningForum.Models
{
    public enum Role 
    {
        Guest,
        Regular,
        Moderator,
        Admin
    }
}
