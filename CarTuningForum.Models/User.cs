﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace CarTuningForum.Models
{
    public class User : IdentityUser<string>
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        //public Role Role { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string Password { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool IsBanned { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsAdmin { get; set; }
        public Role RoleEnum { get; set; }
        //public UserManager<User> UserManager { get; set; }
        //public SignInManager<User> SignInManager { get; set; }

    }
}
