﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarTuningForum.Models
{
    public class Comment
    {
        public int Id { get; set; }
        public string AuthorId { get; set; }
        public User Author { get; set; }
        public Post Post { get; set; }
        public int PostId { get; set; }
        public string Message { get; set; }
        public int Likes { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool IsDeleted { get; set; }

        public IEnumerable<Reply> Replies { get; set; } = new List<Reply>();
    }
}
