﻿using System;

namespace CarTuningForum.Models
{
    public class Reply
    {
        public int Id { get; set; }
        public string AuthorId { get; set; }
        public User Author { get; set; }
        public int CommentId { get; set; }
        public int PostId { get; set; }

        public string Message { get; set; }
        public DateTime CreatedOn { get; set; }
        public int Likes { get; set; }
        public bool IsDeleted { get; set; }
    }
}
