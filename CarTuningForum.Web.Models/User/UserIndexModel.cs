﻿using CarTuningForum.DTOs;
using System.Collections.Generic;

namespace CarTuningForum.Web.ViewModels.User
{
    public class UserIndexModel
    {
        public IEnumerable<UserDTO> UserList { get; set; }
    }
}
