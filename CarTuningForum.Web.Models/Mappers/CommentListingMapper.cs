﻿using CarTuningForum.DTOs;
using CarTuningForum.Web.ViewModels.Comment;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CarTuningForum.Web.ViewModels.Mappers
{
    public static class CommentListingMapper
    {
        public static CommentListingModel GetListingModel(this CommentDTO commentDTO)
        {
            if (commentDTO == null)
            {
                throw new ArgumentException("Null value passed.");
            }
            return new CommentListingModel
            {
                Id = commentDTO.Id,
                AuthorName = commentDTO.Author.FirstName + " " + commentDTO.Author.LastName,
                AuthorId = commentDTO.AuthorId,
                Likes = commentDTO.Likes,
                Message = commentDTO.Message,
            };
        }

        public static CommentDTO GetPostDTO(this CommentListingModel commentListingModel)
        {
            if (commentListingModel == null)
            {
                throw new ArgumentException("Null value passed.");
            }
            return new CommentDTO
            {
                Id = commentListingModel.Id,
                AuthorId = commentListingModel.AuthorId,
                Likes = commentListingModel.Likes,
                Message = commentListingModel.Message

            };
        }
        public static IEnumerable<CommentListingModel> GetListingModel(this ICollection<CommentDTO> posts)
        {
            return posts.Select(GetListingModel).ToList();
        }
    }
}
