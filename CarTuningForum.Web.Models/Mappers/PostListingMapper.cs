﻿using CarTuningForum.DTOs;
using CarTuningForum.DTOs.DTOMappers;
using CarTuningForum.Web.ViewModels.Post;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarTuningForum.Web.ViewModels.Mappers
{
    public static class PostListingMapper
    {
        public static PostListingModel GetListingModel(this PostDTO postDTO)
        {
            if (postDTO == null)
            {
                throw new ArgumentException("Null value passed.");
            }
            return new PostListingModel
            {
                Id = postDTO.Id,
                Title = postDTO.Title,
                AuthorName = postDTO.Author.FirstName + " " + postDTO.Author.LastName,
                AuthorId = postDTO.AuthorId,
                Content = postDTO.Content,
                Comments = (IEnumerable<CommentDTO>)postDTO.Comments,
                Likes = postDTO.Likes,
            };
        }

        public static PostDTO GetPostDTO(this PostListingModel postListingModel)
        {
            if (postListingModel == null)
            {
                throw new ArgumentException("Null value passed.");
            }
            return new PostDTO
            {
                Id = postListingModel.Id,
                Title = postListingModel.Title,
                AuthorId = postListingModel.AuthorId,
                Content = postListingModel.Content,
                Comments = (ICollection<Models.Comment>)postListingModel.Comments.Select(c => c.GetComment()),
                Likes = postListingModel.Likes,
            };
        }
        public static IEnumerable<PostListingModel> GetListingModel(this ICollection<PostDTO> posts)
        {
            return posts.Select(GetListingModel).ToList();
        }
    }
}
