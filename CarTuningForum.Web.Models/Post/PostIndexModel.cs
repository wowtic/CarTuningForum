﻿using CarTuningForum.DTOs;
using System.Collections.Generic;

namespace CarTuningForum.Web.ViewModels.Post
{
    public class PostIndexModel
    {
        public IEnumerable<PostDTO> PostList { get; set; }
    }
}
