﻿using CarTuningForum.DTOs;
using CarTuningForum.Web.ViewModels.Comment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarTuningForum.Web.ViewModels.Post
{
    public class PostListingModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string AuthorName { get; set; }
        public int AuthorId { get; set; }
        public int Likes { get; set; }
        public string DatePosted { get; set; }
        public IEnumerable<CommentDTO> Comments { get; set; }/* = new List<Comment>();*/
        public IEnumerable<ReplyDTO> Replies { get; set; }/* = new List<Reply>();*/
    }
}
