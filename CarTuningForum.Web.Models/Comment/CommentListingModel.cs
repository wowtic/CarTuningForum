﻿using CarTuningForum.Web.ViewModels.Post;

namespace CarTuningForum.Web.ViewModels.Comment

{
    public class CommentListingModel
    {
        public int Id { get; set; }
        public int PostId { get; set; }
        public string Message { get; set; }
        public string AuthorName { get; set; }
        public int AuthorId { get; set; }
        public int Likes { get; set; }
        public string DatePosted { get; set; }
        public PostListingModel Post { get; set; }
    }
}
