﻿using CarTuningForum.DTOs;
using System.Collections.Generic;

namespace CarTuningForum.Web.ViewModels.Comment
{
    public class CommentIndexModel
    {
        public IEnumerable<PostDTO> PostList { get; set; }

    }
}
