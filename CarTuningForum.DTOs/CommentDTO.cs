﻿using CarTuningForum.Models;
using System;
using System.Collections.Generic;

namespace CarTuningForum.DTOs 
{ 
    public class CommentDTO
    {
        public int Id { get; set; }
        public string AuthorId { get; set; }
        public User Author { get; set; }
        public int TopicId { get; set; }
        public Post Post { get; set; }
        public int PostId { get; set; }
        public string Message { get; set; }
        public int Likes { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool IsDeleted { get; set; }
        public IEnumerable<ReplyDTO> Replies { get; set; }/* = new List<Reply>();*/
    }
}
