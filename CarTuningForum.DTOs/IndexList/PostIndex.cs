﻿using CarTuningForum.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarTuningForum.DTOs.IndexList
{
    public class PostIndex
    {
        public IEnumerable<PostDTO> PostList { get; set; }
    }
}
