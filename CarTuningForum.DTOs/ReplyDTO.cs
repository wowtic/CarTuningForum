﻿using CarTuningForum.Models;
using System;

namespace CarTuningForum.DTOs
{
    public class ReplyDTO
    {
        public int Id { get; set; }
        public int AuthorId { get; set; }
        public User Author { get; set; }
        public int CommentId { get; set; }
        public Comment Comment { get; set; }
        public string Message { get; set; }
        public DateTime CreatedOn { get; set; }
        public int Likes { get; set; }
        public bool IsDeleted { get; set; }
    }
}
