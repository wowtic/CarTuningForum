﻿using CarTuningForum.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CarTuningForum.DTOs.DTOMappers
{
    public static class PostDTOMapper
    {
        public static PostDTO GetDTO(this Post post)
        {
            if (post == null)
            {
                throw new ArgumentException("Null value passed.");
            }
            return new PostDTO
            {
                Id = post.Id,
                Title = post.Title,
                Author = post.Author,
                AuthorId = post.AuthorId,
                Content = post.Content,
                Comments = post.Comments.Select(c => c.GetDTO()),
                Likes = post.Likes,
                CreatedOn = post.CreatedOn,
                IsDeleted = post.IsDeleted,
            };
        }

        public static Post GetPost(this PostDTO postDTO)
        {
            if (postDTO == null)
            {
                throw new ArgumentException("Null value passed.");
            }
            return new Post
            {
                Id = postDTO.Id,
                Title = postDTO.Title,
                Author = postDTO.Author,
                AuthorId = postDTO.AuthorId,
                Content = postDTO.Content,
                Comments = (ICollection<Comment>)postDTO.Comments,
                Likes = postDTO.Likes,
                CreatedOn = postDTO.CreatedOn,
                IsDeleted = postDTO.IsDeleted,
            };
        }
        public static IEnumerable<PostDTO> GetDTO(this IEnumerable<Post> posts)
        {
            return posts.Select(GetDTO).ToList();
        }
    }
}
