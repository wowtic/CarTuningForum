﻿using CarTuningForum.DTOs;
using CarTuningForum.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CarTuningForum.DTOs.DTOMappers
{
    public static class CommentDTOMapper
    {
        public static CommentDTO GetDTO(this Comment c)
        {
            if (c == null)
            {
                throw new ArgumentException("Null value passed.");
            }
            return new CommentDTO
            {
                PostId = c.PostId,
                Message = c.Message,
                AuthorId = c.AuthorId,
                Likes = c.Likes,
                CreatedOn = c.CreatedOn,
                IsDeleted = c.IsDeleted,
                Replies = c.Replies.Select(r => r.GetDTO()),
            };
        }

        public static Comment GetComment(this CommentDTO c)
        {
            if (c == null)
            {
                throw new ArgumentException("Null value passed.");
            }
            return new Comment
            {
                PostId = c.PostId,
                Message = c.Message,
                AuthorId = c.AuthorId,
                Likes = c.Likes,
                CreatedOn = c.CreatedOn,
                IsDeleted = c.IsDeleted,
                Replies = c.Replies.Select(r => r.GetReply()),
            };
        }
        public static IEnumerable<CommentDTO> GetDTO(this IEnumerable<Comment> comments)
        {
            return comments.Select(GetDTO).ToList();
        }
    }
}
