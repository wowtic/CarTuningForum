﻿using CarTuningForum.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CarTuningForum.DTOs.DTOMappers
{
    public static class ReplyDTOMapper
    {
        public static ReplyDTO GetDTO(this Reply r)
        {
            if (r == null)
            {
                throw new ArgumentException("Null value passed.");
            }
            return new ReplyDTO
            {
                Id = r.Id,
                CommentId = r.CommentId,
                Message = r.Message,
                Author = r.Author,
                Likes = r.Likes,
                CreatedOn = r.CreatedOn,
                IsDeleted = r.IsDeleted,

            };
        }
        public static Reply GetReply(this ReplyDTO r)
        {
            if (r == null)
            {
                throw new ArgumentException("Null value passed.");
            }
            return new Reply
            {
                Id = r.Id,
                CommentId = r.CommentId,
                Message = r.Message,
                Author = r.Author,
                Likes = r.Likes,
                CreatedOn = r.CreatedOn,
                IsDeleted = r.IsDeleted,
            };
        }
        public static ICollection<ReplyDTO> GetDTO(this ICollection<Reply> replies)
        {
            return replies.Select(GetDTO).ToList();
        }
    }
} 
