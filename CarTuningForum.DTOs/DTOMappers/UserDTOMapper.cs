﻿using CarTuningForum.DTOs;
using CarTuningForum.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CarTuningForum.DTOs.DTOMappers
{
    public static class UserDTOMapper
    {
        public static UserDTO GetDTO(this User u)
        {
            if (u == null)
            {
                throw new ArgumentException("Null value passed.");
            }
            return new UserDTO
            {
                Id = u.Id,
                Username = u.UserName,
                FirstName = u.FirstName,
                LastName = u.LastName,
                CreatedOn = u.CreatedOn,
                IsDeleted = u.IsDeleted,
                IsAdmin = u.IsAdmin,
                IsBanned = u.IsBanned,
                //Role = u.Role,
            };
        }
        public static User GetUser(this UserDTO u)
        {
            if (u == null)
            {
                throw new ArgumentException("Null value passed.");
            }
            return new User
            {
                Id = u.Id,
                UserName = u.Username,
                FirstName = u.FirstName,
                LastName = u.LastName,
                CreatedOn = u.CreatedOn,
                IsDeleted = u.IsDeleted,
                IsAdmin = u.IsAdmin,
                IsBanned = u.IsBanned,
            };
        }



        public static ICollection<UserDTO> GetDTO(this ICollection<User> users)
        {
            return users.Select(GetDTO).ToList();
        }

    }
}
