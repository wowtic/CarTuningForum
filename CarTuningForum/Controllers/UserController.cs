﻿using CarTuningForum.Models;
using CarTuningForum.Service.Contracts;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarTuningForum.Web.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        public IActionResult Details(string userId)
        {
            var user = this._userService.GetUser(userId);
            return View(model: user);
        }
        //public IActionResult Login()
        //{
            
        //}
    }
}
