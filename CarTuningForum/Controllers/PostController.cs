﻿using CarTuningForum.DTOs.IndexList;
using CarTuningForum.DTOs;
using CarTuningForum.Service.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using CarTuningForum.Models;
using Microsoft.AspNetCore.Identity;
using CarTuningForum.ViewModels;
using System.Threading.Tasks;
using System.Security.Claims;
using System;
using CarTuningForum.ViewModels.Mappers;

namespace CarTuningForum.Web.Controllers
{
    public class PostController : Controller
    {
        private readonly IForumService _forumService;
        private readonly IPostService _postService;
        //private readonly UserManager<User> _userManager;
        IUserService _userManager;
        public PostController(IForumService forumService, IPostService postService, /*UserManager<User> */IUserService userManager)
        {
            _forumService = forumService;
            _postService = postService;
            _userManager = userManager;
        }

        public IActionResult Index()
        {
            try
            {
                IEnumerable<PostDTO> postsDTO = _forumService.GetAllPosts();

                var posts = new PostIndex
                {
                    PostList = postsDTO
                };
                return View(model: posts);
            }
            catch (System.NullReferenceException)
            {
                this.Response.StatusCode = StatusCodes.Status404NotFound;
                return View();
            }
        }

        public IActionResult Details(int postId)
        {
            var post = _forumService.GetPostById(postId);
            var comments = _forumService.GetAllPostComments(postId);

            return View(model: post);
        }
        //public IActionResult Like(string userId, int postId)
        //{
        //    this._forumService.LikePost(userId, postId);
        //    return Ok();
        //}
        //public IActionResult Create()
        //{
        //    var postDTO = new PostDTO();
        //    var model = new NewPostModel
        //    {
        //        Title = postDTO.Title,
        //        Author = postDTO.Author.FirstName + " " + postDTO.Author.LastName,
        //        Content = postDTO.Content,
        //        PostId = postDTO.Id,
        //    };
        //    return View(model);
        //}
        //[HttpPost]
        //public async Task<IActionResult> AddPost(NewPostModel model)
        //{
        //    //var userId = this._userManager.GetUser
        //    //var user = await this._userManager.FindByIdAsync(userId);
        //    var postDTO = BuildPost(model);

        //    await this._postService.Create(postDTO);

        //    return RedirectToAction("Details", "Post", postDTO.Id);
        //}

        //private PostDTO BuildPost(NewPostModel model)
        //{
        //    return new PostDTO
        //    {
        //        Title = model.Title,
        //        Content = model.Content,
        //        CreatedOn = DateTime.Now,
        //        //Author = user
        //    };
        //}
    }
}


