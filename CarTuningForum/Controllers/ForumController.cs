﻿using CarTuningForum.Service.Contracts;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarTuningForum.Web.Controllers
{
    public class ForumController : Controller
    {
        private readonly IForumService _forumService;
        private readonly IPostService _postService;
        private readonly ICommentService _commentService;
        public ForumController(IForumService forumService, IPostService postService, ICommentService commentService)
        {
            _forumService = forumService;
            _postService = postService;
            _commentService = commentService;
        }
        public IActionResult Index()
        {
            return View();
        }
    }
}
