﻿using CarTuningForum.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;


namespace CarTuningForum.DataBase
{
    public class ApplicationDbContext : IdentityDbContext/*<User, Role, int>*/
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        //public DbSet<Role> Roles { get; set; }
        public new DbSet<User> Users { get; set; }
        public DbSet<Reply> Replies { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Comment> Comments { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            //builder.ApplyConfiguration(new BeerConfig());
            //builder.ApplyConfiguration(new BreweryConfig());
            //builder.ApplyConfiguration(new ReviewConfig());
            //builder.ApplyConfiguration(new WishListConfig());
            //builder.ApplyConfiguration(new DrankListConfig());

            //builder.Entity<Role>().HasData(
            //    new Role
            //    {
            //        Id = 1,
            //        Name = "member",
            //        NormalizedName = "MEMBER",
            //    },
            //    new Role
            //    {
            //        Id = 2,
            //        Name = "admin",
            //        NormalizedName = "ADMIN",
            //    });

            var hasher = new PasswordHasher<User>();

            User admin = new User
            {
                Id = "1",
                UserName = "admin@admin.admin",
                NormalizedUserName = "ADMIN@ADMIN.ADMIN",
                Email = "admin@admin.admin",
                NormalizedEmail = "ADMIN@ADMIN.ADMIN",
                SecurityStamp = "7I5VHIJTSZNOT3KDWKNFUV5PVYBHGXN",
            };

            admin.PasswordHash = hasher.HashPassword(admin, "Admin123!");

            builder.Entity<User>().HasData(admin);

            base.OnModelCreating(builder);

            base.OnModelCreating(builder);

        }
    }
}