﻿using CarTuningForum.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarTuningForum.Repository.Contracts
{
    public interface IPostRepository
    {
        IEnumerable<Post> GetPosts();
        Post GetPostsByID(int id);
        Task InsertPost(Post post);
        Task CommentPost(User user,Post postToComment, Comment comment);
        Task DeletePost(User user,Post post);
        Task Like(User user,Post post);
        Task Dislike(User user,Post post);
        Task LikeComment(User user,Post post,Comment comment);
        Task DislikeComment(User user,Post post,Comment comment);
        Task UpdatePost(User user,Post post);
        Task Save();
        Task UpdatePostComment(User user, Post postToComment, Comment comment);
        Task DeletePostComment(User user, Post postToComment, Comment comment);
        Comment GetCommentInPostById(int postId,int commentId);


    }
}
