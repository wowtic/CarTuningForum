﻿using CarTuningForum.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarTuningForum.Repository.Contracts
{
    public interface IReplyRepository
    {
        IEnumerable<Reply> GetReplies();
        Reply GetReplyByID(int id);
        Task InsertReply(Reply reply);
        Task DeleteReply(Reply reply);
        Task UpdateReply(Reply reply);
        Task Save();
    }
}
