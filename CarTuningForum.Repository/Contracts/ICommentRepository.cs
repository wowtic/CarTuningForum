﻿using CarTuningForum.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarTuningForum.Repository.Contracts
{
    public interface ICommentRepository
    {
        IEnumerable<Comment> GetComments();
        Comment GetCommentById(int id);
        IEnumerable<Comment> GetAllPostCommentsByPostId(int id);
        Task InsertComment(Comment comment);
        Task DeleteComment(Comment comment);
        Task UpdateComment(Comment comment);
        Task Save();
    }
}
