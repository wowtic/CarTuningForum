﻿using CarTuningForum.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarTuningForum.Repository.Contracts
{
    public interface IUserRepository
    {
        IEnumerable<User> GetUsers();
        IEnumerable<Post> GetUserPosts(User user);
        User GetUserByID(string id);
        Task InsertUser(User user);
        Task DeleteUser(string userId);
        Task BanUser(User user);
        void UpdateUser(User user);
        IEnumerable<Comment> GetUserComments(User user);
        Task Save();
        bool IsUserAdmin(User user);
        bool IsUserRegistered(string userId);
        bool IsUserAuthor(User user, Comment comment);
        bool IsUserAuthor(User user, Post comment);
    }
}
