﻿using CarTuningForum.DataBase;
using CarTuningForum.Models;
using CarTuningForum.Repository.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarTuningForum.Repository
{
    public class CommentRepository : ICommentRepository
    {
        private readonly ApplicationDbContext _context;

        public CommentRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task DeleteComment(Comment comment)
        {
            var deleted = this._context
                .Comments
                .Where(c => c == comment)
                .FirstOrDefault();
            deleted.IsDeleted = true;
            await this.Save();
        }

        public IEnumerable<Comment> GetAllPostCommentsByPostId(int id)
        {
            return this._context
                .Posts
                .Where(p => p.Id == id)
                .First()
                .Comments;
        }

        public Comment GetCommentById(int id)
        {
            var comment = this._context
                .Comments
                .Where(c => c.Id == id && c.IsDeleted == false)
                .FirstOrDefault();

            return comment;
        }

        public IEnumerable<Comment> GetComments()
        {
            return this._context
                .Comments
                .ToList();
        }
        
        public async Task InsertComment(Comment comment)
        {
            this._context
                .Comments
                .Add(comment);
            await this.Save();
        }

        public async Task Save()
        {
            await this._context
                .SaveChangesAsync();
        }

        public async Task UpdateComment(Comment comment)
        {
            this._context
               .Entry(comment)
               .State = EntityState.Modified;
            await this.Save();
        }

        //private bool disposed = false;
        //protected virtual void Dispose(bool disposing)
        //{
        //    if (!this.disposed)
        //    {
        //        if (disposing)
        //        {
        //            this._context.Dispose();
        //        }
        //    }
        //    this.disposed = true;
        //}

        //public void Dispose()
        //{
        //    Dispose(true);
        //    GC.SuppressFinalize(this);
        //}
    }
}
