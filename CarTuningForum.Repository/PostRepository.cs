﻿using CarTuningForum.DataBase;
using CarTuningForum.Models;
using CarTuningForum.Repository.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarTuningForum.Repository
{
    public class PostRepository : IPostRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly IUserRepository _userRepository;
        private readonly ICommentRepository _commentRepository;

        public PostRepository(ApplicationDbContext context, IUserRepository userRepository, ICommentRepository commentRepository)
        {
            _context = context;
            _userRepository = userRepository;
            _commentRepository = commentRepository;
        }
        public async Task DeletePost(User user,Post post)
        {
            this._userRepository.IsUserAuthor(user, post);
            var deleted = this._context
                .Posts
                .Where(p => p.Id == post.Id)
                .FirstOrDefault();
            deleted.IsDeleted = true;
            await this.Save();
        }

        public IEnumerable<Post> GetPosts()
        {
            return this._context
                .Posts
                 .Where(p => p.IsDeleted == false)
                .Include(p => p.Author)
                .Include(p => p.Comments).ThenInclude(c => c.Author)
                .Include(p => p.Replies).ThenInclude(r => r.Author);
        }

        public Post GetPostsByID(int id)
        {
            var post = this._context
                .Posts
                .Where(p => p.Id == id && p.IsDeleted == false)
                .Include(p => p.Author)
                .Include(p => p.Comments).ThenInclude(c => c.Author)
                .Include(p => p.Replies).ThenInclude(r => r.Author)
                .FirstOrDefault();

            return post;
        }
        public async Task Like(User user,Post post)
        {
            this._userRepository.IsUserRegistered(user.Id);
            var dbPost = this._context
                .Posts
                .FirstOrDefault(p => p.Id == post.Id);
            dbPost.Likes++;
            await this.Save();
        }

        public async Task InsertPost(Post post)
        {
            this._userRepository.IsUserRegistered(post.AuthorId);
            var posts = this._context
                .Posts
                .ToList();
            for (int i = 0; i < posts.Count; i++)
            {
                if (posts[i].Title == post.Title)
                {
                    throw new ArgumentException("Post with this Title already exist!");
                }
            }
            this._context
                .Posts
                .Add(post);
            await this.Save();
        }

        public async Task Save()
        {
            await this._context
                .SaveChangesAsync();
        }

        public async Task UpdatePost(User user,Post post)
        {
            
            var dbPost = this._context
                .Posts
                .FirstOrDefault(p => p.Id == post.Id);
            if (this._userRepository.IsUserAuthor(user, dbPost))
            {
                dbPost = post;
            }
            await this.Save();
        }

        public async Task Dislike(User user, Post post)
        {
            this._userRepository.IsUserRegistered(user.Id);
            var dbPost = this._context
                .Posts
                .FirstOrDefault(p => p.Id == post.Id);
            dbPost.Likes++;
            await this.Save();
        }


        public async Task LikeComment(User user, Post post, Comment comment)
        {
            this._userRepository.IsUserRegistered(user.Id);
            var dbPost = this._context
                .Posts
                .FirstOrDefault(p => p.Id == post.Id);
            var dbComment = dbPost
                .Comments
                .FirstOrDefault(c => c.Id == comment.Id);
            dbComment.Likes++;
            await this._commentRepository.UpdateComment(dbComment);
            await this._commentRepository.Save();
            await this.Save();
        }

        public async Task DislikeComment(User user, Post post, Comment comment)
        {
            this._userRepository.IsUserRegistered(user.Id);
            var dbPost = this._context
                .Posts
                .FirstOrDefault(p => p.Id == post.Id);
            var dbComment = dbPost
                .Comments
                .FirstOrDefault(c => c.Id == comment.Id);
            dbComment.Likes--;
            await this._commentRepository.UpdateComment(dbComment);
            await this._commentRepository.Save();
            await this.Save();
        }
        public async Task CommentPost(User user, Post postToComment, Comment comment)
        {
            this._userRepository.IsUserRegistered(user.Id);
            var post = this._context
                .Posts
                .FirstOrDefault(p => p.Id == postToComment.Id);
            post.Comments.Append(comment);
            await this._commentRepository.InsertComment(comment);
            await this.Save();

        }

        public async Task UpdatePostComment(User user, Post post, Comment comment)
        {
            this._userRepository.IsUserAuthor(user,comment);
            var dbPost = this._context
                .Posts
                .FirstOrDefault(p => p.Id == post.Id);

            var dbComment = dbPost
                .Comments
                .FirstOrDefault(c => c.Id == comment.Id);
            dbComment = comment;

            await this._commentRepository.UpdateComment(dbComment);
            await this._commentRepository.Save();
            await this.Save();
        }

        public async Task DeletePostComment(User user, Post post, Comment comment)
        {
            this._userRepository.IsUserAuthor(user, comment);
            var dbPost = this._context
                .Posts
                .FirstOrDefault(p => p.Id == post.Id);
            var dbComment = dbPost
                .Comments
                .FirstOrDefault(c => c.Id == comment.Id);
            dbComment.IsDeleted = true;

            await this._commentRepository.UpdateComment(dbComment);
            await this._commentRepository.Save();
            await this.Save();
        }

        public Comment GetCommentInPostById(int postId,int commentId)
        {
            return this.GetPostsByID(postId)
                .Comments
                .FirstOrDefault(c => c.Id == commentId);
        }
        //public IEnumerable<Comment> GetCommentsInPostById(int postId)
        //{
        //    return this.GetPostsByID(postId)
        //        .Comments
        //        .Where(c => c.PostId == postId)
        //        .Include()
        //}
    }
}
