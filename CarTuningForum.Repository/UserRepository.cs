﻿using CarTuningForum.DataBase;
using CarTuningForum.Models;
using CarTuningForum.Repository.Contracts;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarTuningForum.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly ApplicationDbContext _context;
        //private readonly IdentityUser _user;

        public UserRepository(ApplicationDbContext context /*, IdentityUser user*/)
        {
            this._context = context;
            //this._user = user;
        }
        public async Task DeleteUser(string userId)
        {
            if (!this.IsUserRegistered(userId))
            {
                throw new ArgumentException("Not existing user!");
            }
            
            var userToDelete = this._context
                .Users
                .FirstOrDefault(u => u.Id == userId);
            userToDelete.IsDeleted = true;
            await this.Save();
        }
        public async Task BanUser(User user)
        {
            var banned = this._context
                .Users
                .FirstOrDefault(u => u.Id == user.Id);
            user.IsBanned = true;
            await this.Save();
        }

        public User GetUserByID(string userId)
        {
            //var user = this._user.
            //var user = this._context
            //    .Users
            //    .Where(u => u.Id == userId)
            //    .FirstOrDefault();
            var user = this._context
                .Users
                .Where(u => u.Id == userId)
                .FirstOrDefault();
            return user;
        }

        public IEnumerable<User> GetUsers()
        {
            return this._context
                .Users
                .Where(u => u.IsDeleted == false && u.IsBanned == false)
                .ToList();
        }

        public async Task InsertUser(User user)
        {
            var checkIfExist = this._context
                .Users
                .Where(u => u == user)
                .FirstOrDefault();
            if (user != null)
            {
                throw new ArgumentException("This user already exist!");
            }
            else
            {
                this._context
                    .Users
                    .Add(user);
                await this.Save();
            }
        }

        public async Task Save()
        {
            await this._context
                .SaveChangesAsync();
        }

        public void UpdateUser(User user)
        {
            this._context
               .Entry(user)
               .State = EntityState.Modified;
        }
        public IEnumerable<Comment> GetUserComments(User user)
        {
            return this._context
                .Comments
                .Where(c => c.AuthorId == user.Id && c.IsDeleted == false)
                .Include(c => c.Replies)
                .ThenInclude(r => r.Author)
                .ToList();
        }
        public bool IsUserRegistered(string userId)
        {
            var userValidate = this._context
                .Users
                .FirstOrDefault(u => u.Id == userId);
            if (userValidate == null)
            {
                throw new ArgumentException("This is not existing user!");
            }
            if (userValidate.IsBanned)
            {
                throw new ArgumentException("This user is banned!");
            }
            if (userValidate.IsDeleted)
            {
                throw new ArgumentException("This user is deleted!");
            }
            return true;
        }
        public bool IsUserAdmin(User user)
        {
            var userToValidate = this._context
                .Users
                .FirstOrDefault(u => u.Id == user.Id);
            if (userToValidate == null)
            {
                throw new ArgumentException("This is not existing user!");
            }
            //if (userToValidate.RoleId == 2)
            //{
            //    return true;
            //}
            return false;
        }
        public bool IsUserAuthor(User user, Comment comment)
        {
            if (user.Id == comment.AuthorId)
            {
                return true;
            }
            throw new ArgumentException("This is not the author");
        }
        public bool IsUserAuthor(User user, Post comment)
        {
            if (user == comment.Author)
            {
                return true;
            }
            throw new ArgumentException("This is not the author");
        }

        public IEnumerable<Post> GetUserPosts(User user)
        {
            return this._context
                .Posts
                .Where(p => p.Author == user);
        }
    }
}
