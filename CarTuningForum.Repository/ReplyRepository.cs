﻿using CarTuningForum.DataBase;
using CarTuningForum.Models;
using CarTuningForum.Repository.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarTuningForum.Repository
{
    public class ReplyRepository : IReplyRepository
    {
        private readonly ApplicationDbContext _context;
        public ReplyRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task DeleteReply(Reply reply)
        {
            var deleted = this._context
                 .Replies
                 .Where(r => r == reply)
                 .FirstOrDefault();
            deleted.IsDeleted = true;
            await this.Save();
        }

        public IEnumerable<Reply> GetReplies()
        {
            return this._context
                .Replies
                .Where(r => r.IsDeleted == false)
                .Include(r => r.Author)
                .Include(r => r.CreatedOn)
                .ToList();
        }

        public Reply GetReplyByID(int id)
        {
            return this._context
                .Replies
                .Where(r => r.Id == id)
                .FirstOrDefault();
        }

        public async Task InsertReply(Reply reply)
        {
            this._context
                .Replies
                .Add(reply);
            await this.Save();
        }

        public async Task Save()
        {
            await this._context
                .SaveChangesAsync();
        }

        public async Task UpdateReply(Reply reply)
        {
            this._context
               .Entry(reply).State = EntityState.Modified;
            await this.Save();
        }
    }
}
