﻿using CarTuningForum.DTOs;
using CarTuningForum.DTOs.DTOMappers;
using CarTuningForum.Repository.Contracts;
using CarTuningForum.Service.Contracts;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarTuningForum.Service
{
    public class ReplyService : IReplyService
    {
        private readonly IReplyRepository _replyRepository;
        public ReplyService(IReplyRepository replyRepository)
        {
            this._replyRepository = replyRepository;
        }

        public async Task<ReplyDTO> Create(ReplyDTO replyDTO)
        {
            await this._replyRepository.InsertReply(replyDTO.GetReply());
            await this._replyRepository.Save();
            return replyDTO;
        }

        public async Task<ReplyDTO> DeleteComment(int replyId)
        {
            var reply = this._replyRepository.GetReplyByID(replyId);
            reply.IsDeleted = false;
            await this._replyRepository.UpdateReply(reply);
            await this._replyRepository.Save();
            return reply.GetDTO();
        }

        public async Task<ReplyDTO> Dislike(int replyId)
        {
            var reply = this._replyRepository.GetReplyByID(replyId);
            reply.Likes--;
            await this._replyRepository.UpdateReply(reply);
            await this._replyRepository.Save();
            return reply.GetDTO();
        }

        public IEnumerable<ReplyDTO> GetAll()
        {
            return this._replyRepository.GetReplies().Select(r => r.GetDTO());
        }

        public ReplyDTO GetById(int id)
        {
            return this._replyRepository.GetReplyByID(id).GetDTO();

        }

        public async Task<ReplyDTO> Like(int replyId)
        {
            var reply = this._replyRepository.GetReplyByID(replyId);
            reply.Likes++;
            await this._replyRepository.UpdateReply(reply);
            await this._replyRepository.Save();
            return reply.GetDTO();
        }

        public async Task<ReplyDTO> UpdateComment(int replyId, string newMessage)
        {
            var reply = this._replyRepository.GetReplyByID(replyId);
            reply.Message = newMessage;
            await this._replyRepository.UpdateReply(reply);
            await this._replyRepository.Save();
            return reply.GetDTO();
        }
    }
}

