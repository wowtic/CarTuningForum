﻿using CarTuningForum.DTOs;
using CarTuningForum.DTOs.DTOMappers;
using CarTuningForum.Models;
using CarTuningForum.Repository.Contracts;
using CarTuningForum.Service.Contracts;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarTuningForum.Service
{
    public class PostService : IPostService
    {
        private readonly IPostRepository _postRepository;
        private readonly IUserRepository _userRepository;

        public PostService(IPostRepository postRepository, IUserRepository userRepository)
        {
            _postRepository = postRepository;
            _userRepository = userRepository;
        }

        public async Task<Post> Create(PostDTO postDTO)
        {
            var post = postDTO.GetPost();
            await this._postRepository.InsertPost(post);
            await this._postRepository.Save();
            return post;
        }

        public async Task DeletePost(string userId,int postId)
        {
            var user = this._userRepository.GetUserByID(userId);
            var post = this._postRepository.GetPostsByID(postId);
            await this._postRepository.DeletePost(user,post);
            await this._postRepository.Save();
        }

        public async Task<Post> DislikePost(string userId,int postId)
        {
            var user = this._userRepository.GetUserByID(userId);
            var post = this._postRepository.GetPostsByID(postId);
            await this ._postRepository.Like(user,post);
            await this._postRepository.Save();
            return post;
        }

        public async Task<Post> EditPostTitle(string userId,int postId, string newTitle)
        {
            var user = this._userRepository.GetUserByID(userId);
            var post = this._postRepository.GetPostsByID(postId);
            post.Title = newTitle;
            await this._postRepository.UpdatePost(user,post);
            await this._postRepository.Save();
            return post;
        }

        public IEnumerable<PostDTO> GetAll()
        {
            return this._postRepository.GetPosts().Select(p => p.GetDTO());
        }

        public PostDTO GetById(int id)
        {
            return this._postRepository.GetPostsByID(id).GetDTO();
        }

        public async Task<Post> LikePost(string userId,int postId)
        {
            var user = this._userRepository.GetUserByID(userId);
            var post = this._postRepository.GetPostsByID(postId);
            post.Likes++;
            await this ._postRepository.UpdatePost(user,post);
            await this._postRepository.Save();
            return post;
        }

        public async Task<Post> UpdatePostMessage(string userId,int postId, string newMessage)
        {
            var user = this._userRepository.GetUserByID(userId);
            var post = this._postRepository.GetPostsByID(postId);
            post.Content = newMessage;
            await this ._postRepository.UpdatePost(user,post);
            await this._postRepository.Save();
            return post;
        }
    }
}
