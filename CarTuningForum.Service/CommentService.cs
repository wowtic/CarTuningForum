﻿using CarTuningForum.DTOs;
using CarTuningForum.DTOs.DTOMappers;
using CarTuningForum.Repository.Contracts;
using CarTuningForum.Service.Contracts;
using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarTuningForum.Service
{
    public class CommentService : ICommentService
    {
        private readonly ICommentRepository _commentRepository;
        public CommentService(ICommentRepository commentRepository)
        {
            this._commentRepository = commentRepository;
        }

        public async Task<CommentDTO> Create(CommentDTO commentDTO)
        {

            await this._commentRepository.InsertComment(commentDTO.GetComment());

            await this._commentRepository.Save();

            return commentDTO;
        }
        [Authorize("")]
        public IEnumerable<CommentDTO> GetAll()
        {
           return this._commentRepository.GetComments().Select(c => c.GetDTO());

        }
        public CommentDTO GetById(int id)
        {
            var comment = this._commentRepository.GetCommentById(id);
            return comment.GetDTO();
        }

        
        public async Task<CommentDTO> Like(int commentId)
        {
            var comment = this._commentRepository.GetCommentById(commentId);
            comment.Likes++;
            await this._commentRepository.UpdateComment(comment);
            await this._commentRepository.Save();
            return comment.GetDTO();
        }
        public async Task<CommentDTO> Dislike(int commentId)
        {
            var comment = this._commentRepository.GetCommentById(commentId);
            comment.Likes--;
            await this._commentRepository.UpdateComment(comment);
            await this._commentRepository.Save();
            return comment.GetDTO();
        }


        public async Task<CommentDTO> CreateComment(CommentDTO commentDTO)
        {

            await this._commentRepository.InsertComment(commentDTO.GetComment());
            await this._commentRepository.Save();
            return commentDTO;
        }

        public async Task<CommentDTO> UpdateComment(int commentId, string newMessage)
        {

            var comment = this._commentRepository.GetCommentById(commentId);
            comment.Message = newMessage;
            await this._commentRepository.UpdateComment(comment);
            await this._commentRepository.Save();
            return comment.GetDTO();
        }

        public async Task<CommentDTO> DeleteComment(int commentId)
        {
            var comment = this._commentRepository.GetCommentById(commentId);
            comment.IsDeleted = true;
            await this._commentRepository.UpdateComment(comment);
            await this._commentRepository.Save();
            return comment.GetDTO();
        }
    }
}
