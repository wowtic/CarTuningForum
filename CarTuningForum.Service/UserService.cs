﻿using CarTuningForum.DTOs;
using CarTuningForum.DTOs.DTOMappers;
using CarTuningForum.Repository.Contracts;
using CarTuningForum.Service.Contracts;
using System.Collections.Generic;
using System.Linq;

namespace CarTuningForum.Service
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        public UserService(IUserRepository userRepository)
        {
            this._userRepository = userRepository;
        }
        public UserDTO BanUser(string userId)
        {
            var user = this._userRepository.GetUserByID(userId);

            this._userRepository.BanUser(user);
            return user.GetDTO();
        }

        public UserDTO DeleteUser(string userId)
        {
            var user = this._userRepository.GetUserByID(userId);
            this._userRepository.DeleteUser(userId);
            return user.GetDTO();
        }

        public string GetName(string userId)
        {
            return this.GetUser(userId).FirstName;
        }

        public UserDTO GetUser(string userId)
        {
            var user = this._userRepository.GetUserByID(userId);
            return user.GetDTO();
        }


        public IEnumerable<UserDTO> GetAllUsers()
        {
            var users = this._userRepository.GetUsers();
            return users.Select(u => u.GetDTO());
        }
        public UserDTO CreateUser(UserDTO userDTO)
        {
            var user = userDTO.GetUser();
            this._userRepository.InsertUser(user);
            return user.GetDTO();
        }
        public IEnumerable<CommentDTO> GetUserComments(string userId)
        {
            var user = this._userRepository
                .GetUserByID(userId);

            return this._userRepository
                .GetUserComments(user)
                .Select(c => c.GetDTO()); 
        }


        //public IActionResult Login()
        //{
        //}

        //[Authorize]
        //public ActionResult Logout()
        //{
        //}
    }
}
