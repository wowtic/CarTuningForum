﻿using CarTuningForum.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarTuningForum.Service.Contracts
{
    public interface IReplyService
    {
        Task<ReplyDTO> UpdateComment(int replyId, string newMessage);
        Task<ReplyDTO> Like(int replyId);
        Task<ReplyDTO> Dislike(int replyId);
        Task<ReplyDTO> DeleteComment(int replyId);
        Task<ReplyDTO> Create(ReplyDTO replyDTO);
        ReplyDTO GetById(int id);
        IEnumerable<ReplyDTO> GetAll();

    }
}
