﻿using CarTuningForum.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarTuningForum.Service.Contracts
{
    public interface ICommentService
    {
        Task<CommentDTO> UpdateComment(int commentId, string newMessage);
        Task<CommentDTO> Like(int commentId);
        Task<CommentDTO> Dislike(int commentId);
        Task<CommentDTO> DeleteComment(int postId);
        Task<CommentDTO> Create(CommentDTO commentDTO);
        CommentDTO GetById(int id);
        IEnumerable<CommentDTO> GetAll();


    }
}
