﻿using CarTuningForum.DTOs;
using CarTuningForum.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarTuningForum.Service.Contracts
{
    public interface IForumService
    {
        IEnumerable<User> GetAllActiveUsers();
        IEnumerable<PostDTO> GetAllUserPosts(string userId);
        IEnumerable<CommentDTO> GetAllUserComments(string userId);
        Task<IEnumerable<ReplyDTO>> GetAllUserReplies(string userId);

        IEnumerable<PostDTO> GetAllPosts();
        PostDTO GetPostById(int postId);
        Task<PostDTO> LikePost(string userId, int postId);
        Task<PostDTO> DislikePost(string userId, int postId);
        Task<PostDTO> UpdatePostTitle(string userId,int postId, string newTitle);
        Task<PostDTO> UpdatePostContent(string userId,int postId, string newMessage);
        Task DeletePost(string userId,int postId);

        IEnumerable<CommentDTO> GetAllPostComments(int postId);
        Task<PostDTO> CommentPost(string userId, int postId, CommentDTO comment);
        Task<CommentDTO> LikeComment(string userId,int postId, int commentId);
        Task<CommentDTO> DislikeComment(string userId, int postId, int commentId);
        Task<CommentDTO> UpdateComment(string userId,int postId, int commentId, string newMessage);
        Task DeleteComment(string userId,int postId, int commentId);


    }
}
