﻿using CarTuningForum.DTOs;
using CarTuningForum.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CarTuningForum.Service.Contracts

{
    public interface IPostService
    {
        PostDTO GetById(int id);
        IEnumerable<PostDTO> GetAll();
        Task<Post> Create(PostDTO postDTO);
        Task DeletePost(string userId,int postId);
        Task<Post> EditPostTitle(string userId,int postId, string newTitle);
        Task<Post> UpdatePostMessage(string userId,int postId, string newMessage);
        Task<Post> LikePost(string userId,int postId);
        Task<Post> DislikePost(string userId,int postId);
    }
}