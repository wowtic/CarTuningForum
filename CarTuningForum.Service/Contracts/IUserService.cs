﻿using CarTuningForum.DTOs;
using System.Collections.Generic;

namespace CarTuningForum.Service.Contracts
{
    public interface IUserService
    {
        public UserDTO BanUser(string userId);
        public UserDTO DeleteUser(string userId);
        public string GetName(string userId);
        public UserDTO GetUser(string userId);
        public IEnumerable<UserDTO> GetAllUsers();
        public UserDTO CreateUser(UserDTO userDTO);
        public IEnumerable<CommentDTO> GetUserComments(string userId);
    }
}
