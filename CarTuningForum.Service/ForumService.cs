﻿using CarTuningForum.DTOs;
using CarTuningForum.DTOs.DTOMappers;
using CarTuningForum.Models;
using CarTuningForum.Repository.Contracts;
using CarTuningForum.Service.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarTuningForum.Service
{
    public class ForumService : IForumService
    {
        private readonly IPostRepository _postRepository;
        private readonly ICommentRepository _commentRepository;
        private readonly IUserRepository _userRepository;
        public ForumService(IPostRepository postRepository, ICommentRepository commentRepository, IUserRepository userRepository)
        {
            this._postRepository = postRepository;
            this._commentRepository = commentRepository;
            this._userRepository = userRepository;
        }

        public async Task<PostDTO> CommentPost(string userId, int postId, CommentDTO comment)
        {
            var user = this._userRepository.GetUserByID(userId);
            var post = this._postRepository.GetPostsByID(postId);
            await this._postRepository.CommentPost(user,post, comment.GetComment());
            return post.GetDTO();
        }

        public async Task DeleteComment(string userId, int postId, int commentId)
        {
            var user = this._userRepository.GetUserByID(userId);
            var post = this._postRepository.GetPostsByID(postId);
            var comment = post
                .Comments
                .FirstOrDefault(c => c.Id == commentId);
            await this._postRepository.DeletePostComment(user, post, comment);

        }

        public async Task DeletePost(string userId, int postId)
        {
            var user = this._userRepository.GetUserByID(userId);
            var post = this._postRepository.GetPostsByID(postId);
            await this._postRepository.DeletePost(user, post);
        }

        public async Task<CommentDTO> DislikeComment(string userId,int postId,int commentId)
        {
            var user = this._userRepository.GetUserByID(userId);
            var post = this._postRepository.GetPostsByID(postId);
            var comment = this._postRepository.GetCommentInPostById(postId, commentId);
            await this._postRepository.DislikeComment(user, post, comment);
            return comment.GetDTO();
        }

        public async Task<PostDTO> DislikePost(string userId, int postId)
        {
            var user = this._userRepository.GetUserByID(userId);
            var post = this._postRepository.GetPostsByID(postId);
            await this._postRepository.Like(user, post);
            return post.GetDTO();
        }

        public IEnumerable<User> GetAllActiveUsers()
        {
            return this._userRepository.GetUsers();
        }

        public IEnumerable<CommentDTO> GetAllPostComments(int postID)
        {
            return this._commentRepository
                .GetAllPostCommentsByPostId(postID).Select(c => c.GetDTO());
        }

        public IEnumerable<PostDTO> GetAllPosts()
        {
            return this._postRepository.GetPosts().Select(p => p.GetDTO());
        }

        public IEnumerable<CommentDTO> GetAllUserComments(string userId)
        {

            var user = this._userRepository.GetUserByID(userId);
            var comments = this._userRepository.GetUserComments(user);
            return comments.Select(c => c.GetDTO());
        }

        public IEnumerable<PostDTO> GetAllUserPosts(string userId)
        {
            var user = this._userRepository.GetUserByID(userId);
            return this._userRepository.GetUserPosts(user).Select(p => p.GetDTO());
        }

        public Task<IEnumerable<ReplyDTO>> GetAllUserReplies(string userId)
        {
            throw new NotImplementedException();
        }

        public PostDTO GetPostById(int postId)
        {
            return this._postRepository.GetPostsByID(postId).GetDTO();
        }

        public async Task<CommentDTO> LikeComment(string userId, int postId, int commentId)
        {
            var user = this._userRepository.GetUserByID(userId);
            var post = this._postRepository.GetPostsByID(postId);
            var comment = this._postRepository.GetCommentInPostById(postId,commentId);
            await this._postRepository.LikeComment(user, post, comment);
            return comment.GetDTO();
        }

        public async Task<PostDTO> LikePost(string userId, int postId)
        {
            var user = this._userRepository.GetUserByID(userId);
            var post = this._postRepository.GetPostsByID(postId);
            await this._postRepository.Like(user, post);
            return post.GetDTO();
        }

        public async Task<CommentDTO> UpdateComment(string userId, int postId, int commentId, string newMessage)
        {
            var user = this._userRepository.GetUserByID(userId);
            var post = this._postRepository.GetPostsByID(postId);
            var comment = this._postRepository.GetCommentInPostById(postId,commentId);
            comment.Message = newMessage;
            await this._postRepository.UpdatePostComment(user, post, comment);
            return comment.GetDTO();
        }

        public async Task<PostDTO> UpdatePostContent(string userId, int postId, string newMessage)
        {
            var user = this._userRepository.GetUserByID(userId);
            var post = this._postRepository.GetPostsByID(postId);
            post.Content = newMessage;
            await this._postRepository.UpdatePost(user, post);
            return post.GetDTO();
        }

        public async Task<PostDTO> UpdatePostTitle(string userId, int postId, string newTitle)
        {
            var user = this._userRepository.GetUserByID(userId);
            var post = this._postRepository.GetPostsByID(postId);
            post.Title = newTitle;
            await this._postRepository.UpdatePost(user, post);
            return post.GetDTO();
        }
    }
}
