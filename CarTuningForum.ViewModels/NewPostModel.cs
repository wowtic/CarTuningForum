﻿using System;

namespace CarTuningForum.ViewModels
{
    public class NewPostModel
    {
        public string Title { get; set; }
        public string Author { get; set; }
        public string Content { get; set; }
        public int PostId { get; set; }
    }
}
