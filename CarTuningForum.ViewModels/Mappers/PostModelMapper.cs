﻿using CarTuningForum.DTOs;
using CarTuningForum.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarTuningForum.ViewModels.Mappers
{
    public static class PostModelMapper
    {
        public static PostDTO BuildPost(this NewPostModel model, User user)
        {
            return new PostDTO
            {
                Title = model.Title,
                Content = model.Content,
                CreatedOn = DateTime.Now,
                Author = user
            };
        }
            
    }
}
